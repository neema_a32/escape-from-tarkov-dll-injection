# PolyHook - x86/x64 Hooking Library
**Provides abstract C++ interface  for various hooking methods**

#Hooking Methods*:

1. **_Detour_**
  * Description: Modifies opcode to jmp to hook and allocates a trampoline for jmp back
  * Length Disassembler Support (Capstone)
  * Supports Code Relocation, including EIP/RIP relative instructions

2. **_Virtual Function Detour_** : 
  * Description: Detours the function pointed to by the Vtable

3. **_Virtual Function Pointer Swap_** 
  * Description: Swaps the pointer in the Vtable to your hook
  
4. **_Virtual Table Pointer Swap_**
  * Description: Swaps the Vtable pointer after copying pointers in source Vtable, 
  then swaps virtual function pointer in the new copy

5. **Import Address Table**
  * Description: Swaps pointer in the input address table to the hook

* All methods support x86 and x64
* Relies on modified capstone branch https://github.com/stevemk14ebr/capstone
* More Information can be found at the wiki to the right

Credits to DarthTon, evolution536, Dogmatt

Additonal Authors: Arveen Emdad, Nick Kunes

#LICENSE:
BSD 2-Clause (Simplified BSD)
