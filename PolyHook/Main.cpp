/* ======================= detours ======================= */

#include "PolyHook.hpp"
#include "Main.h"

/* ======================= windows ======================= */

#include <fstream>
#include <vector>
#include <deque>
#include <string>
#include <math.h>

/* ======================= macros ======================= */

#include "FW1FontWrapper/FW1FontWrapper.h"

BOOL UNHOOKING = FALSE;

VOID fnExitThread () {
	UNHOOKING = TRUE;
	Sleep(2000);
	presentHook->UnHook();
	Sleep(2000);
	if (!FreeConsole()) {
		FreeLibrary(mainModule);
	}
}

ID3D11Device *pDevice = NULL;
ID3D11DeviceContext *pContext = NULL;
ID3D11RenderTargetView* RenderTargetView = NULL;
IFW1Factory *pFW1Factory = NULL;
IFW1FontWrapper *pFontWrapper = NULL;
BOOL isFirst = TRUE;

HRESULT WINAPI hkPresent(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags)
{
	if (!UNHOOKING) {
		if (isFirst)
		{

			printf("present hook init\n");
			pSwapChain->GetDevice(__uuidof(pDevice), (void**)&pDevice);
			pDevice->GetImmediateContext(&pContext);
			FW1CreateFactory(FW1_VERSION, &pFW1Factory);
			pFW1Factory->CreateFontWrapper(pDevice, L"Tahoma", &pFontWrapper);
			pContext->OMSetRenderTargets(1, &RenderTargetView, NULL);
			pFW1Factory->Release();
			isFirst = FALSE;
		}

		if (GetAsyncKeyState(VK_END) & 1) {
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)fnExitThread, NULL, NULL, NULL);
			return pPresent(pSwapChain, SyncInterval, Flags);
		}
		/**/
		//CHEAT START

		if (pFontWrapper) {

			DWORD64 gameWorld = NULL, fpsCamera = NULL, taggedObjects = NULL, lastTagged = NULL;

			DWORD64 base = (DWORD64)GetModuleHandle(NULL);
			DWORD64 gom = *(DWORD64*)(base + 0x14327E0);

			if (gom) {

				DWORD64 activeObjects = *(DWORD64*)(gom + 0x18);
				DWORD64 lastActive = *(DWORD64*)(gom + 0x10);
				if (activeObjects) {
					while (lastActive != activeObjects) {
						activeObjects = *(DWORD64*)(activeObjects + 0x8);
						DWORD64 curObject = *(DWORD64*)(activeObjects + 0x10);
						DWORD64 nextObjectNamePtr = *(DWORD64*)(curObject + 0x60);
						if (nextObjectNamePtr) {
							char* name = (char*)(nextObjectNamePtr);

							if (strcmp(name, "GameWorld") == 0) {
								gameWorld = curObject;
								//printf("%I64u\n", gameWorld);
								goto tagged;
							}

						}
					}
					goto longjump;
				}

			tagged:
				taggedObjects = *(DWORD64*)(gom + 0x8);
				lastTagged = *(DWORD64*)(gom + 0x0);
				if (taggedObjects) {
					while (lastTagged != taggedObjects) {
						taggedObjects = *(DWORD64*)(taggedObjects + 0x8);
						DWORD64 curTagged = *(DWORD64*)(taggedObjects + 0x10);
						DWORD64 nextTaggedName = *(DWORD64*)(curTagged + 0x60);
						char* taggedName = (char*)(nextTaggedName);
						printf("%s\n", taggedName);

						if (strcmp(taggedName, "FPS Camera") == 0) {
							fpsCamera = curTagged;
							break;
						}

					}
				}

			longjump:
				if (gameWorld && fpsCamera) {
					DWORD64 component = *(DWORD64*)(gameWorld + 0x30);
					DWORD64 networker = *(DWORD64*)(component + 0x18);
					DWORD64 script = *(DWORD64*)(networker + 0x28);
					DWORD64 regPlayer = *(DWORD64*)(script + 0x60);
					DWORD64 pArray = *(DWORD64*)(regPlayer + 0x10);
					DWORD size = *(DWORD*)(regPlayer + 0x18);

					for (int i = 0; i < size; i++) {
						DWORD64 player = *(DWORD64*)(pArray + (0x20 + (i * 0x8)));

						if (player) {
							DWORD64 profile = *(DWORD64*)(player + 0x400);
							DWORD64 pInfo = *(DWORD64*)(profile + 0x28);
							DWORD64 playerNamePtr = *(DWORD64*)(pInfo + 0x10);
							UnicodeString* playerName = (UnicodeString*)(playerNamePtr);
							//wprintf(L"%s\n", playerNamePtr.data);
							if (TRUE) {



								DWORD64 context = *(DWORD64*)(player + 0x58);
								if (context) {
									DirectX::XMFLOAT3 locPlayerPos = *(DirectX::XMFLOAT3*)(context + 0x60);

									DirectX::XMFLOAT3 point = WorldToScreen(locPlayerPos, fpsCamera);
									DirectX::XMFLOAT3 cameraPosition = *(DirectX::XMFLOAT3*)((*(DWORD64*)((*(DWORD64*)((*(DWORD64*)(fpsCamera + 0x30)) + 0x8)) + 0x38)) + 0x90);
									long double myDistance = sqrt(pow(cameraPosition.x - locPlayerPos.x, 2.0) + pow(cameraPosition.y - locPlayerPos.y, 2.0) + pow(cameraPosition.z - locPlayerPos.z, 2.0));
									//Camera Game Object + 0x30 ] + 0x8 ] + 0x38 ] + 0x90 = Camera Position
									WCHAR lahey[256];
									swprintf(lahey, L"%s %.2f", player ? playerName->data : L"AI", std::trunc(100 * myDistance) / 100);
									//ternary notation == checkbox ? killneema : killnom
									pFontWrapper->DrawString(pContext, lahey, 14, point.x, point.y, 0xffff1612, FW1_RESTORESTATE);
								}
							}
						}
					}
				}
			}

			pFontWrapper->DrawString(pContext, L"tarcock", 18, 16.0f, 16.0f, 0xffff1612, FW1_RESTORESTATE);

		}

		//CHEAT END
	}
	
	return pPresent(pSwapChain, SyncInterval, Flags);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH: {
		mainModule = hModule;
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Hook, NULL, NULL, NULL);
	}
	break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
