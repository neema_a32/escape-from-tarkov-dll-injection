#include "Scanner.h"
#include <Psapi.h>
#include <Windows.h>
#include <iostream>

using namespace Hack;

#define INRANGE(x,a,b)		(x >= a && x <= b) 
#define getBits( x )		(INRANGE(x,'0','9') ? (x - '0') : ((x&(~0x20)) - 'A' + 0xa))
#define getByte( x )		(getBits(x[0]) << 4 | getBits(x[1]))

#define SIG_LANDSCAPE_SCENERY "8B 85 ? ? ? ? 89 44 24 28 F3 0F 10 88 ? ? ? ?"
#define SIG_MUZZLE "8B 41 04 8B 50 04 85 D2 74 0E"
#define SIG_WORLD_LOCALPLAYER "8B 0D ? ? ? ? 8B 81 ? ? ? ? 8B"
#define SIG_WORLD_FASTVEHICLESVISIBLENEAR "8D 89 ? ? ? ? E8 ? ? ? ? 8B 0D ? ? ? ? 53 E8 ? ? ? ? 8B 0D ? ? ? ?"
#define SIG_LANDSCAPE_QUADTREEROOTEX_GET "E8 ? ? ? ? 8B E8 8B 5D ? 85 DB"
#define SIG_LANDSCAPE_GETHEIGHT "83 EC 10 8B 89 ? ? ? ?"
#define SIG_LANDSCAPE "8B 0D ?? ?? ?? ?? 85 C9 74 1A D9 44 24 0C"
#define SIG_ISDESTROYED "80 BF ? ? ? ? ? F3 0F 10 15 ? ? ? ? F3"
#define SIG_ISTYPED "E8 ? ? ? ? 83 C4 ? 85 C0 75 14 8B 4C 24 10"
#define SIG_ADDITEM "E8 ? ? ? ? 8B 0D ? ? ? ? 53 E8 ? ? ? ? 8B 0D"
#define SIG_DROPITEM "56 8B 74 24 08 57 8B 7C 24 10 57 8B 06 56 FF 90 ? ? ? ? 84 C0 74 3C E8 ? ? ? ? 8B C8 8B 10 8B 82 ? ? ? ? FF D0 84 C0 75 18 E8 ? ? ? ? 57 56 8B C8 8B 10 FF 92 ? ? ? ? 5F B0 01"
#define SIG_ITEMINHANDS "FF 50 04 85 ED 74 05"
#define SIG_PLAYER_GEAR "8B 8B ? ? ? ? 56 57 6A ?"
#define SIG_PLAYER_REMOTEPLAYER "FF B1 ? ? ? ? 8B CA 8B 80 ? ? ? ?"
#define SIG_NETWORK_MANAGER "B9 ? ? ? ? E8 ? ? ? ? 8B 44 24 04 C3"
#define SIG_WORLD "83 3D ? ? ? ? ? 74 19"
#define SIG_SHOT_NEWSHOT "56 57 8B 7C 24 10 33 F6 8B CF"
#define SIG_LANDSCAPE_QUADTREEEXROOT "8D 85 ? ? ? ? 8D 64 24 ?"
#define SIG_NETWORKCLIENT_CREATEOBJECT "83 EC 10 56 8B 74 24 18 57 8B F9 85 F6 74 6D"
#define SIG_MAN_ANIMATEPOINT "81 EC ? ? ? ? 53 55 56 8B D9 8D 4C 24 34"
#define SIG_ENTITY_GETVISUALSTATE "83 3D ? ? ? ? ? 56 8B F1 77 35"
#define SIG_ENTITY_SETFUTUREPOSITION "56 FF 74 24 08 8B F1 8B 4E 48 8B 01 FF 10"
#define SIG_ENTITY_TYPEBANK "B9 ? ? ? ? 6A ? 68 ? ? ? ? 68 ? ? ? ? 8B ?"

PBYTE Scanner::findPattern(const PBYTE rangeStart, const PBYTE rangeEnd, const char* pattern)
{
	const unsigned char* pat = reinterpret_cast<const unsigned char*>(pattern);
	PBYTE firstMatch = 0;
	for (PBYTE pCur = rangeStart; pCur < rangeEnd; ++pCur) {
		if (*(PBYTE)pat == (BYTE)'\?' || *pCur == getByte(pat)) {
			if (!firstMatch) {
				firstMatch = pCur;
			}
			pat += (*(PWORD)pat == (WORD)'\?\?' || *(PBYTE)pat != (BYTE)'\?') ? 3 : 2;
			if (!*pat) {
				return firstMatch;
			}
		}
		else if (firstMatch) {
			pCur = firstMatch;
			pat = reinterpret_cast<const unsigned char*>(pattern);
			firstMatch = 0;
		}
	}
	return NULL;
}

DWORD Scanner::RunTests(BOOL &async)
{
	MODULEINFO modinfo;
	PBYTE Address;
	GetModuleInformation(GetCurrentProcess(), GetModuleHandle("DayZ.exe"), &modinfo, sizeof(MODULEINFO));


	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_LANDSCAPE_SCENERY);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Scenery: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_MUZZLE);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Muzzle: %x\n", *(DWORD*)((Address + 0x11) + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_WORLD_LOCALPLAYER);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("LocalPlayer: %x\n", *(DWORD*)((Address + 0x6) + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_WORLD_FASTVEHICLESVISIBLENEAR);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("FastVehiclesTable: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_LANDSCAPE_QUADTREEROOTEX_GET);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("QuadTreeExRoot::Get: %x\n", *(DWORD*)(Address + 1) + 5 + Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_LANDSCAPE_GETHEIGHT);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Landscape::GetHeight: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_LANDSCAPE);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Landscape: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ISDESTROYED);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("IsDestroyed: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ISTYPED);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Entity::IsTyped: %x\n", *(DWORD*)(Address + 1) + 5 + Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ADDITEM);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("EntityList::AddItem: %x\n", *(DWORD*)(Address + 1) + 5 + Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_DROPITEM);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Entity::DropItem: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ITEMINHANDS);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("ItemInHands: %x\n", *(DWORD*)((Address + 0x1C) + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_PLAYER_GEAR);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Gear: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_PLAYER_REMOTEPLAYER);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("RemotePlayer: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_NETWORK_MANAGER);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("NetworkManager: %x\n", *(DWORD*)(Address + 1));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_WORLD);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("World: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_SHOT_NEWSHOT);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("NewShot: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_LANDSCAPE_QUADTREEEXROOT);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Objects: %x\n", *(DWORD*)(Address + 2));

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_NETWORKCLIENT_CREATEOBJECT);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("NetworkClient::CreateObject: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_MAN_ANIMATEPOINT);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Man::AnimatePoint: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ENTITY_GETVISUALSTATE);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Entity::GetVisualState: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ENTITY_SETFUTUREPOSITION);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("Entity::SetFuturePosition: %x\n", Address);

	Address = findPattern((PBYTE)modinfo.lpBaseOfDll, (PBYTE)((DWORD)modinfo.lpBaseOfDll + modinfo.SizeOfImage), SIG_ENTITY_TYPEBANK);
	if (IsBadReadPtr(Address, 16))
		return NULL;
	printf("TypeBank: %x\n", *(DWORD*)(Address + 1));

	async = FALSE;
	return TRUE;
}
