#include <Windows.h>
#include <d3d11.h>
#include <iostream>
#include <string>
#include <inttypes.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <fstream>

#pragma comment(lib, "d3d11.lib")

#include "Scanner.h"


typedef HRESULT(__stdcall *Present) (IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);
HRESULT WINAPI hkPresent(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);


Present pPresent = NULL;
HMODULE mainModule;
PLH::Detour* presentHook;

class UnicodeString
{
public:
private:
	char pad_0x0000[0x10]; //0x0000
public:
	__int32 len; //0x0010
	wchar_t data[40]; //0x0014

}; //Size=0x0064

bool Compare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for (; *szMask; ++szMask, ++pData, ++bMask)
		if (*szMask == 'x' && *pData != *bMask)   return 0;
	return (*szMask) == NULL;
}

DWORD Pattern(DWORD dwAddress, DWORD dwLen, BYTE *bMask, char * szMask)
{
	for (DWORD i = 0; i<dwLen; i++)
		if (Compare((BYTE*)(dwAddress + i), bMask, szMask))  return (DWORD)(dwAddress + i);
	return 0;
}

VOID Hook(void)
{
	AllocConsole();
	freopen("CONOUT$", "w", stdout);

	HWND hWnd = GetForegroundWindow();
	IDXGISwapChain* pSwapChain;

	ID3D11Device *pDevice = NULL;
	ID3D11DeviceContext *pContext = NULL;

	DWORD_PTR* pSwapChainVtable = NULL;
	DWORD_PTR* pContextVTable = NULL;
	DWORD_PTR* pDeviceVTable = NULL;

	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hWnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.Windowed = ((GetWindowLongPtr(hWnd, GWL_STYLE) & WS_POPUP) != 0) ? false : true;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &pSwapChain, &pDevice, NULL, &pContext));

	pSwapChainVtable = (DWORD_PTR*)pSwapChain;
	pSwapChainVtable = (DWORD_PTR*)pSwapChainVtable[0];

	pContextVTable = (DWORD_PTR*)pContext;
	pContextVTable = (DWORD_PTR*)pContextVTable[0];

	pDeviceVTable = (DWORD_PTR*)pDevice;
	pDeviceVTable = (DWORD_PTR*)pDeviceVTable[0];

	presentHook = new PLH::Detour();
	presentHook->SetupHook((PBYTE)pSwapChainVtable[8], (PBYTE)hkPresent);
	presentHook->Hook();
	pPresent = presentHook->GetOriginal<Present>();

	pDevice->Release();
	pContext->Release();
	pSwapChain->Release();

}

void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	horizontal = desktop.right;
	vertical = desktop.bottom;
}


DirectX::XMFLOAT3 WorldToScreen(DirectX::XMFLOAT3 origin, DWORD64 fpsCamera)
{
	DirectX::XMMATRIX temp;
	DirectX::XMMATRIX viewMatrix = *(DirectX::XMMATRIX*)((*(DWORD64*)((*(DWORD64*)(fpsCamera + 0x30)) + 0x18)) + 0xC0);

	temp = DirectX::XMMatrixTranspose(viewMatrix);

	DirectX::XMFLOAT3 translationVector = DirectX::XMFLOAT3(temp._41, temp._42, temp._43);
	DirectX::XMFLOAT3 up = DirectX::XMFLOAT3(temp._21, temp._22, temp._23);
	DirectX::XMFLOAT3 right = DirectX::XMFLOAT3(temp._11, temp._12, temp._13);

	float w = *DirectX::XMVector3Dot(XMLoadFloat3(&translationVector), XMLoadFloat3(&origin)).vector4_f32 + temp._44;

	//if (w < 0.098f)
	//return  D3DXVECTOR3(0,0,0);

	if (w < 1.f)
		w = 1.f;

	float y = *DirectX::XMVector3Dot(XMLoadFloat3(&up), XMLoadFloat3(&origin)).vector4_f32 + temp._24;
	float x = *DirectX::XMVector3Dot(XMLoadFloat3(&right), XMLoadFloat3(&origin)).vector4_f32 + temp._14;

	int ScreenWidth = 0;
	int ScreenHeight = 0;
	GetDesktopResolution(ScreenWidth, ScreenHeight);

	float ScreenX = (ScreenWidth / 2) * (1.f + x / w);
	float ScreenY = (ScreenHeight / 2) * (1.f - y / w);
	float ScreenZ = w;

	return DirectX::XMFLOAT3(ScreenX, ScreenY, ScreenZ);
}