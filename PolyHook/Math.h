#pragma once

#include <algorithm>

namespace DayZ
{
	class Vector3D
	{
	private:
		float x, y, z;
	public:

		Vector3D(float X = 0, float Y = 0, float Z = 0)
		{
			x = X;
			y = Y;
			z = Z;
		}
		~Vector3D() {};

		Vector3D operator*(float num) const
		{
			return Vector3D(x * num, y * num, z * num);
		}

		friend Vector3D operator*(float num, Vector3D const &vec)
		{
			return Vector3D(vec.x * num, vec.y * num, vec.z * num);
		}

		Vector3D operator+(const Vector3D &vec) const
		{
			return Vector3D(x + vec.x, y + vec.y, z + vec.z);
		}

		Vector3D operator-(const Vector3D &vec) const
		{
			return Vector3D(x - vec.x, y - vec.y, z - vec.z);
		}

		float dotVector3D(const Vector3D &vec) const
		{
			return x * vec.x + y * vec.y + z * vec.z;
		}

		Vector3D crossVector3D(const Vector3D &vec) const
		{
			return Vector3D(y * vec.z - z * vec.y,
				z * vec.x - x * vec.z,
				x * vec.y - y * vec.x);
		}

		float getX() const 
		{
			return this->x;
		}

		float getY() const
		{
			return this->y;
		}

		float getZ() const
		{
			return this->z;
		}

		void setX(float X)
		{
			this->x = X;
		}

		void setY(float Y)
		{
			this->y = Y;
		}
	};

	class Matrix4
	{
	private:
		Vector3D m_Rows[4];

	public:
		Matrix4 &operator = (const Matrix4 &old)
		{
			m_Rows[0] = old.m_Rows[0];
			m_Rows[1] = old.m_Rows[1];
			m_Rows[2] = old.m_Rows[2];
			m_Rows[3] = old.m_Rows[3];
			return *this;
		}

		Vector3D &operator [] (int i)
		{
			return m_Rows[i];
		}

		const Vector3D &operator [] (int i) const
		{
			return m_Rows[i];
		}

		Vector3D &getTranslation()
		{
			return m_Rows[3];
		}

		const Vector3D &getTranslation() const
		{
			return m_Rows[3];
		}

		Vector3D &getForward()
		{
			return m_Rows[0];
		}

		const Vector3D &getForward() const
		{
			return m_Rows[0];
		}
	};

	inline Vector3D operator *(const Matrix4 &a, const Vector3D &o)
	{
		return Vector3D(a[0].getX() * o.getX() + a[1].getX() * o.getY() + a[2].getX() * o.getZ() + a.getTranslation().getX(), a[0].getY() * o.getX() + a[1].getY() * o.getY() + a[2].getY() * o.getZ() + a.getTranslation().getY(), a[0].getZ() * o.getX() + a[1].getZ() * o.getY() + a[2].getZ() * o.getZ() + a.getTranslation().getZ());
	}
}