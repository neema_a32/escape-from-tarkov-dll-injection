#pragma once
#include <Windows.h>

namespace Hack
{
	class Scanner
	{
	private:
		static PBYTE findPattern(const PBYTE rangeStart, const PBYTE rangeEnd, const char* pattern);
	public:
		static DWORD RunTests(BOOL&);		
	};
}