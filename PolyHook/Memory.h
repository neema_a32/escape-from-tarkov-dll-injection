#pragma once
#include <Windows.h>
#include <cstdint>
#include <type_traits>

template <typename Type, typename Base, typename Offset>
static inline Type Ptr(Base base, Offset offset)
{
	static_assert(std::is_pointer<Type>::value || std::is_integral<Type>::value, "Type must be a pointer or address");
	static_assert(std::is_pointer<Base>::value || std::is_integral<Base>::value, "Base must be a pointer or address");
	static_assert(std::is_pointer<Offset>::value || std::is_integral<Offset>::value, "Offset must be a pointer or address");

	if (!base) return NULL;

	return base ? (Type)((uint64_t)base + (uint64_t)offset) : Type();
}

template<typename Function> 
Function CallVirtual(PVOID Base, DWORD Index)
{
	PDWORD* VTablePointer = (PDWORD*)Base;
	PDWORD VTableFunctionBase = *VTablePointer;
	DWORD dwAddress = VTableFunctionBase[Index];
	                                                                                                                                                       
	return (Function)(dwAddress);
}

#define OFFSET_CAMERA_VIEWRIGHT                     0x4
#define OFFSET_CAMERA_VIEWUP                        0x10
#define OFFSET_CAMERA_VIEFORWARD                    0x1C
#define OFFSET_CAMERA_VIEWTRANSLATION               0x28
#define OFFSET_CAMERA_VIEWPORTSIZE                  0x54
#define OFFSET_CAMERA_PROJ_D1                       0xCC
#define OFFSET_CAMERA_PROJ_D2                       0xD8

#define ADDRESS_ENTITY_ISTYPED                      0x734CA0
#define OFFSET_ENTITY_RAWVISUALSTATE                0x48
#define OFFSET_ENTITY_LODSHAPE                      0x4C
#define OFFSET_ENTITY_ENTITYTYPE                    0x80
#define OFFSET_ENTITY_ISDESTROYED                   0x2BC
#define VTABLE_INDEX_ENTITY_GETVISUALSTATE          59

#define OFFSET_ENTITYLIST_ADDITEM                   0xAED000 

#define OFFSET_ENTITYTYPE_TYPENAME                  0x34
#define OFFSET_ENTITYTYPE_BASETYPE                  0x44
#define OFFSET_ENTITYTYPE_CLASSNAME                 0x70
#define OFFSET_ENTITYTYPE_CLEANNAME                 0x9A0

#define OFFSET_INGAMEUI_ENTITYYINCROSSHAIR          0x8
#define OFFSET_INGAMEUI_AIMINGPOINT                 0x1C

#define ADDRESS_LANDSCAPE                           0x11A7530
#define ADDRESS_LANDSCAPE_GETHEIGHT                 0xD5C730
#define ADDRESS_LANDSCAPE_QUADTREEROOTEX_GET        0xAC0AE0 
#define OFFSET_LANDSCAPE_SCENERY                    0x3058
#define OFFSET_LANDSCAPE_OBJECTS                    0x3184

#define OFFSET_NETWORKCLIENT_PLAYERS                0xC

#define ADDRESS_NETWORKMANAGER                      0x117CA88
#define OFFSET_NETWORKMANAGER_NETWORKCLIENT         0x28
#define ADDRESS_NETWORKCLIENT_CREATEOBJECT			0x855A60

#define OFFSET_OBJECTLIST_OBJECTS                   0x10

#define ADDRESS_PLAYER_DROPITEM                     0x709920
#define OFFSET_PLAYER_REMOTEPLAYER                  0x978
#define OFFSET_PLAYER_GEAR                          0x7FC
#define OFFSET_PLAYER_ITEMINHANDS                   0xBAC

#define OFFSET_SCENERY_LANDGRID                     0xFC
#define OFFSET_SCENERY_INVERSELANDGRID              0x100
#define OFFSET_SCENERY_LANDRANGE                    0x104
#define OFFSET_SCENERY_LANDRANGEMASK                0x108

#define OFFSET_VISUALSTATE_SPEED                    0x48

#define OFFSET_WEAPON_MUZZLE                        0x850

#define ADDRESS_WORLD                               0x118152C
#define OFFSET_WORLD_INGAMEUI                       0x20 
#define OFFSET_WORLD_INGAMEUIOFFSET                 0x60
#define OFFSET_WOLRD_CAMERA                         0x128
#define OFFSET_WORLD_FASTVEHICLESVISIBLENEAR        0x9B4
#define OFFSET_WORLD_LOCALPLAYER                    0x1690

#define ADDRESS_ENTITYTYPEBANK 0x1180040
#define OFFSET_ENTITYTYPEBANK_TYPES 0x10